package com.s249296.kreatorpostaci;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PointsGiveAway extends AppCompatActivity {
    private int pointsToGive = 16;
    private int ab[];



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points_give_away);
        final ConstraintLayout relativeLayout;
        relativeLayout = findViewById(R.id.pointsT);
        Character c = Character.getInstance();
        if(c.getClassCh().equals("Rogue")){
        relativeLayout.setBackgroundColor(Color.parseColor("#4d0099"));

            findViewById(R.id.buttonMinStr).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonPlStr).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonMinCon).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonPlCon).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonMinDex).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonPlDex).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonMinInt).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonPlInt).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonMinWis).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonPlWis).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonMinChar).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.buttonPlChar).setBackgroundColor(Color.parseColor("#003cb3"));
            findViewById(R.id.button8).setBackgroundColor(Color.parseColor("#003cb3"));
        }
        else if(c.getClassCh().equals("Archer")){
            relativeLayout.setBackgroundColor(Color.parseColor("#73cc65"));
            findViewById(R.id.buttonMinStr).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonPlStr).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonMinCon).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonPlCon).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonMinDex).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonPlDex).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonMinInt).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonPlInt).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonMinWis).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonPlWis).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonMinChar).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.buttonPlChar).setBackgroundColor(Color.parseColor("#149a83"));
            findViewById(R.id.button8).setBackgroundColor(Color.parseColor("#149a83"));
        }
        else if(c.getClassCh().equals("Mage")){
            relativeLayout.setBackgroundColor(Color.parseColor("#0099ff"));
            findViewById(R.id.buttonMinStr).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonPlStr).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonMinCon).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonPlCon).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonMinDex).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonPlDex).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonMinInt).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonPlInt).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonMinWis).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonPlWis).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonMinChar).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.buttonPlChar).setBackgroundColor(Color.parseColor("#0099cc"));
            findViewById(R.id.button8).setBackgroundColor(Color.parseColor("#0099cc"));
        }
        ab = new int[6];
        ab[0] = 8;
        ab[1] = 8;
        ab[2] = 8;
        ab[3] = 8;
        ab[4] = 8;
        ab[5] = 8;
        Button minusFirstAb = findViewById(R.id.buttonMinStr);
        Button plusFirstAb = findViewById(R.id.buttonPlStr);
        TextView firstAb = findViewById(R.id.textViewStr);
        Button minusSecAb = findViewById(R.id.buttonMinDex);
        Button plusSecAb = findViewById(R.id.buttonPlDex);
        TextView secAb = findViewById(R.id.textViewDex);
        Button minusThrdAb = findViewById(R.id.buttonMinCon);
        Button plusThrdAb = findViewById(R.id.buttonPlCon);
        TextView thrdAb = findViewById(R.id.textViewCon);
        Button minusFrthAb = findViewById(R.id.buttonMinInt);
        Button plusFrthAb = findViewById(R.id.buttonPlInt);
        TextView frthAb = findViewById(R.id.textViewInt);
        Button minusFfthAb = findViewById(R.id.buttonMinWis);
        Button plusFfthAb = findViewById(R.id.buttonPlWis);
        TextView ffthAb = findViewById(R.id.textViewWis);
        Button minusSixAb = findViewById(R.id.buttonMinChar);
        Button plusSixAb = findViewById(R.id.buttonPlChar);
        TextView sixAb = findViewById(R.id.textViewChar);
        TextView pToGive = findViewById(R.id.textView10);
        minusFirstAb.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ab[0]--;
                        if (ab[0] < 0) {
                            ab[0] = 0;
                        } else {
                            pointsToGive++;
                        }
                        firstAb.setText(Integer.toString(ab[0]));
                        pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                    }
                }
        );
        plusFirstAb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pointsToGive <= 0 || ab[0]>=14) {
                    return;
                } else {
                    pointsToGive--;
                    ab[0]++;
                    firstAb.setText(Integer.toString(ab[0]));
                    pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                }
            }

        });
        minusSecAb.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ab[1]--;
                        if (ab[1] < 0) {
                            ab[1] = 0;
                        } else {
                            pointsToGive++;
                        }
                        secAb.setText(Integer.toString(ab[1]));
                        pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                    }
                }
        );
        plusSecAb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pointsToGive <= 0 || ab[1]>=14) {
                    return;
                } else {
                    pointsToGive--;
                    ab[1]++;
                    secAb.setText(Integer.toString(ab[1]));
                    pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                }
            }

        });
        minusThrdAb.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ab[2]--;
                        if (ab[2] < 0) {
                            ab[2] = 0;
                        } else {
                            pointsToGive++;
                        }
                        thrdAb.setText(Integer.toString(ab[2]));
                        pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                    }
                }
        );
        plusThrdAb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pointsToGive <= 0 || ab[2]>=14) {
                    return;
                } else {
                    pointsToGive--;
                    ab[2]++;
                    thrdAb.setText(Integer.toString(ab[2]));
                    pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                }
            }

        });
        minusFrthAb.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ab[3]--;
                        if (ab[3] < 0) {
                            ab[3] = 0;
                        } else {
                            pointsToGive++;
                        }
                        frthAb.setText(Integer.toString(ab[3]));
                        pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                    }
                }
        );
        plusFrthAb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pointsToGive <= 0 || ab[3]>=14) {
                    return;
                } else {
                    pointsToGive--;
                    ab[3]++;
                    frthAb.setText(Integer.toString(ab[3]));
                    pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                }
            }

        });
        minusFfthAb.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ab[4]--;
                        if (ab[4] < 0) {
                            ab[4] = 0;
                        } else {
                            pointsToGive++;
                        }
                        ffthAb.setText(Integer.toString(ab[4]));
                        pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                    }
                }
        );
        plusFfthAb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pointsToGive <= 0 || ab[4]>=14) {
                    return;
                } else {
                    pointsToGive--;
                    ab[4]++;
                    ffthAb.setText(Integer.toString(ab[4]));
                    pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                }
            }

        });
        minusSixAb.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ab[5]--;
                        if (ab[5] < 0) {
                            ab[5] = 0;
                        } else {
                            pointsToGive++;
                        }
                        sixAb.setText(Integer.toString(ab[5]));
                        pToGive.setText("Fucks to give: " + Integer.toString(pointsToGive));
                    }
                }
        );
        plusSixAb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pointsToGive <= 0 || ab[5]>=14) {
                    return;
                } else {
                    pointsToGive--;
                    ab[5]++;
                    sixAb.setText(Integer.toString(ab[5]));
                    pToGive.setText("Points to give: " + Integer.toString(pointsToGive));
                }
            }

        });

        ((Button) findViewById(R.id.button8)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Character c = Character.getInstance();
                for (int i = 0; i < ab.length ; i++)
                    c.setSkills(i, ab[i]);
                Intent intent = new Intent(getApplicationContext(), storytime.class);
                startActivity(intent);
            }
        });
    }
}