package com.s249296.kreatorpostaci;

public class Character {
    private static Character character;
    private String gender, race, name, classCh, ability[];
    private String story;
    private int skills[];

    private Character() {
        ability = new String[2];
        skills = new int[6];
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getStory() {
        return
                story;
    }

    public void setSkills(int idx, int skill) {
        skills[idx] = skill;
    }

    public int getSkill(int idx) {
        return skills[idx];
    }

    public void setGender(String g) {
        gender = g;
    }

    public void setRace(String c) {
        race = c;
    }

    public void setClassCh(String e) {
        classCh = e;
    }

    public void setName(String n) {
        name = n;
    }

    public void setAbility(int idx, String v) {
        ability[idx] = v;
    }

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String getClassCh() {
        return classCh;
    }

    public String getRace() {
        return race;
    }

    public String getAbility(int idx) {
        return ability[idx];
    }

    public String toString() {
        return name + " (" + gender + ") of " + race + " race. Skillful " + classCh + " proficient in [" + ability[0]
                + "] and [" + ability[1] + "]. Stats: Str(" + skills[0] + ")/Dex(" + skills[1] + ")/Con("
                + skills[2] + ")/Int(" + skills[3] + ")/Wis(" + skills[4] + ")/Char("
                + skills[5] + ") - '" + story.replaceAll("[\\t\\n\\r]+", " ") + "'\n";
    }

    public static Character getInstance() {
        if (character != null)
            return character;
        character = new Character();
        return character;
    }
}
