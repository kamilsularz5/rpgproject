package com.s249296.kreatorpostaci;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class LoadCharacter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_character);
        TextView tv = findViewById(R.id.textView6);
        ((Button) findViewById(R.id.buttonLoad)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Character c = Character.getInstance();
                File f = new File(getFilesDir() + "/"+c.getName()+".txt");
                FileInputStream fis = null;
                try {
                    fis=openFileInput(f.getName());
                    InputStreamReader isr = new InputStreamReader(fis);
                    BufferedReader br = new BufferedReader(isr);
                    StringBuilder sb = new StringBuilder();
                    String text;
                    while((text=br.readLine())!=null){
                        sb.append(text).append("\n");
                    }
                    tv.setText(sb.toString());
                }catch (FileNotFoundException e){e.printStackTrace();} catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    if(fis != null){
                        try {
                            fis.close();
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
    });
}}